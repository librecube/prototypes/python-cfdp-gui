import wx

from .toolbar import Toolbar
from .main_panel import MainPanel
from .controller.native import NativeController
from .controller.cfdp import CfdpController


class CfdpGui(wx.Frame):

    def __init__(self, title):
        super().__init__(None, title=title, size=(800, 800))

        self.toolbar = Toolbar(self)
        self.SetToolBar(self.toolbar)
        self.toolbar.Realize()

        self.local_controller = NativeController(self)
        self.remote_controller = CfdpController(self)

        self.main_panel = MainPanel(
            self, self.local_controller, self.remote_controller)

        self.status_bar = wx.StatusBar(self)
        self.SetStatusBar(self.status_bar)


class App:

    def run():
        app = wx.App(False)
        gui = CfdpGui("CFDP File Transfer Application")
        gui.Show(True)
        app.MainLoop()
