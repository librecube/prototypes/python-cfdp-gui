import wx
import yaml
import os


ID_TOOL_CONFIGURE = wx.NewIdRef()
ID_TOOL_LOAD_CONFIG = wx.NewIdRef()
ID_TOOL_SAVE_CONFIG = wx.NewIdRef()
ID_TOOL_CONNECT = wx.NewIdRef()
ID_TOOL_DISCONNECT = wx.NewIdRef()

PROPORTION = 0
BORDER = 5


class Toolbar(wx.ToolBar):

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.locale = wx.Locale(wx.LANGUAGE_ENGLISH)
        self.config = None
        self.path = os.getcwd()
        self.default_path_yaml = None

        self.set_config()

        btn_connect = self.CreateTool(
            ID_TOOL_CONFIGURE, 'configure',
            wx.ArtProvider.GetBitmap(wx.ART_NEW),
            shortHelp="Configure...")
        self.AddTool(btn_connect)

        btn_connect = self.CreateTool(
            ID_TOOL_LOAD_CONFIG, 'load config',
            wx.ArtProvider.GetBitmap(wx.ART_FILE_OPEN),
            shortHelp="Load config")
        self.AddTool(btn_connect)

        btn_connect = self.CreateTool(
            ID_TOOL_SAVE_CONFIG, 'save config',
            wx.ArtProvider.GetBitmap(wx.ART_FILE_SAVE),
            shortHelp="Save current config")
        self.AddTool(btn_connect)

        btn_connect = self.CreateTool(
            ID_TOOL_CONNECT, 'connect',
            wx.ArtProvider.GetBitmap(wx.ART_GO_FORWARD),
            shortHelp="Connect")
        self.AddTool(btn_connect)

        btn_disconnect = self.CreateTool(
            ID_TOOL_DISCONNECT,
            'disconnect', wx.ArtProvider.GetBitmap(wx.ART_CROSS_MARK),
            shortHelp="Disconnect")
        self.AddTool(btn_disconnect)
        self.EnableTool(ID_TOOL_DISCONNECT, False)

        self.Bind(wx.EVT_TOOL, self.on_configure, id=ID_TOOL_CONFIGURE)
        self.Bind(wx.EVT_TOOL, self.on_load_config, id=ID_TOOL_LOAD_CONFIG)
        self.Bind(wx.EVT_TOOL, self.on_save_config, id=ID_TOOL_SAVE_CONFIG)
        self.Bind(wx.EVT_TOOL, self.on_connect, id=ID_TOOL_CONNECT)
        self.Bind(wx.EVT_TOOL, self.on_disconnect, id=ID_TOOL_DISCONNECT)

    def on_configure(self, params):
        with ConfigureConnectionDialog(self.config) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                for key in self.config:
                    self.config[key] = dlg.text[key].Value

    def on_load_config(self, params):
        with wx.FileDialog(self, "Open config.yaml file",
                           wildcard="yaml files (*.yaml)|*.yaml",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST,
                           defaultDir=self.path) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return

            pathname = fileDialog.GetPath()
            try:
                self.open_yaml(pathname)
            except IOError:
                wx.LogError("Cannot open file.")

    def on_save_config(self, params):
        with wx.FileDialog(self, "Save config.yaml file",
                           wildcard="yaml files (*.yaml)|*.yaml",
                           style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT,
                           defaultDir=self.path) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return

            pathname = fileDialog.GetPath()
            try:
                with open(pathname, 'w') as file:
                    yaml.dump(self.config, file, sort_keys=False)
            except IOError:
                wx.LogError("Cannot save current data in file '%s'." %
                            pathname)

    def on_connect(self, params):
        self.parent.main_panel.on_connect(self.config)
        self.EnableTool(ID_TOOL_CONNECT, False)
        self.EnableTool(ID_TOOL_DISCONNECT, True)
        wx.LogStatus(self.parent, "Connected")

    def on_disconnect(self, params):
        self.parent.remote_controller.disconnect()
        self.parent.main_panel.on_disconnect()
        self.EnableTool(ID_TOOL_CONNECT, True)
        self.EnableTool(ID_TOOL_DISCONNECT, False)
        wx.LogStatus(self.parent, "Disconnected")

    def check_yaml(self):
        self.default_path_yaml = os.path.join(self.path, "config.yaml")
        return os.path.exists(self.default_path_yaml)

    def set_config(self):
        if not self.check_yaml():  # if there is no config.yaml
            wx.MessageBox('There is no config.yaml in current '
                          'directory: \n%s\n\nPlease load one.'
                          % (str(self.path)),
                          'Error', wx.FD_OPEN | wx.ICON_WARNING)
        else:
            self.open_yaml(self.default_path_yaml)

    def open_yaml(self, path):
        with open(path) as f:
            self.config = yaml.safe_load(f)
        if self.config['rootpath'] == 'os.getcwd()':
            self.config['rootpath'] = self.path


class ConfigureConnectionDialog(wx.Dialog):

    def __init__(self, config):
        super().__init__(None, title="Connect")

        vbox = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(vbox)
        sizer1 = wx.GridSizer(2, 0, 0)
        self.text = {}

        for key, value in config.items():
            sizer1.Add(
                wx.StaticText(self, 0, key), PROPORTION,
                wx.ALIGN_CENTER_VERTICAL | wx.LEFT | wx.RIGHT | wx.TOP, BORDER)
            self.text[key] = wx.TextCtrl(self, value=str(value))
            sizer1.Add(
                self.text[key], PROPORTION,
                wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, BORDER)

        vbox.Add(sizer1)
        hbox1 = self.CreateStdDialogButtonSizer(flags=(wx.OK | wx.CANCEL))
        vbox.Add(hbox1)
