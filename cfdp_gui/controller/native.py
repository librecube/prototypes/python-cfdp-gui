import os
import shutil
from datetime import datetime

from .base import BaseController


class NativeController(BaseController):

    def __init__(self, parent, rootpath=os.getcwd()):
        self.parent = parent
        self.rootpath = rootpath
        self.current_path = rootpath

    def get_current_virtual_path(self):
        relpath = os.path.relpath(self.current_path, self.rootpath)
        path = '/'.join(os.path.normpath(relpath).split(os.path.sep))
        if path == '.':
            path = '/'
        if not path.startswith('/'):
            path = '/' + path
        return path

    def list_directory(self):
        listing = []

        dir_list = os.scandir(path=os.path.join(self.current_path))

        for entry in dir_list:
            modified = datetime.fromtimestamp(
                entry.stat().st_mtime).strftime('%Y-%m-%d %H:%M:%S')
            size = entry.stat().st_size

            if entry.is_dir():
                listing.append(
                    ('d', entry.name, "{}".format(size), modified))
            elif entry.is_file():
                listing.append(
                    ('f', entry.name, "{}".format(size), modified))

        # do not show hidden folders or files
        listing = [x for x in listing if not x[1].startswith('.')]

        # sort by folders and name
        listing.sort(key=lambda x: (x[0], x[1]))

        # add parent directory
        listing.insert(0, ('d', '..', "---", "---"))

        return listing

    def enter_directory(self, directory):
        self.current_path = os.path.abspath(
            os.path.join(self.current_path, directory))

    def create_file(self, name):
        with open(os.path.join(self.current_path, name), 'wb') as file:
            file.close()

    def delete_file(self, name):
        os.remove(os.path.join(self.current_path, name))

    def rename_file(self, old_name, new_name):
        os.rename(
            os.path.join(self.current_path, old_name),
            os.path.join(self.current_path, new_name))

    def create_directory(self, name):
        os.mkdir(os.path.join(self.current_path, name))

    def remove_directory(self, name):
        shutil.rmtree(os.path.join(self.current_path, name))

    def deny_file(self, name):
        os.remove(os.path.join(self.current_path, name))

    def deny_directory(self, name):
        shutil.rmtree(os.path.join(self.current_path, name))
