import wx

import cfdp

from .native import NativeController
from .cfdp import CfdpController


def transfer_file(name, destination_controller, source_controller):

    if isinstance(source_controller, NativeController) and\
            isinstance(destination_controller, CfdpController):

        source_path = source_controller.get_current_virtual_path()
        if source_path.endswith('/'):
            source_filename = source_path + name
        else:
            source_filename = source_path + '/' + name

        destination_path = destination_controller.get_current_virtual_path()
        if destination_path.endswith('/'):
            destination_filename = destination_path + name
        else:
            destination_filename = destination_path + '/' + name

        wx.LogMessage("Copy local {} to remote {}".format(
            source_filename, destination_filename))

        destination_controller.cfdp_entity.put(
            destination_id=destination_controller.remote_entity_id,
            source_filename=source_filename,
            destination_filename=destination_filename,
            transmission_mode=destination_controller.mode)

    elif isinstance(source_controller, CfdpController) and\
            isinstance(destination_controller, NativeController):

        source_path = source_controller.get_current_virtual_path()
        if source_path.endswith('/'):
            source_filename = source_path + name
        else:
            source_filename = source_path + '/' + name

        destination_path = destination_controller.get_current_virtual_path()
        if destination_path.endswith('/'):
            destination_filename = destination_path + name
        else:
            destination_filename = destination_path + '/' + name

        wx.LogMessage("Copy remote {} to local {}".format(
            source_filename, destination_filename))

        source_controller.cfdp_entity.put(
            destination_id=source_controller.remote_entity_id,
            transmission_mode=source_controller.mode,
            messages_to_user=[
                cfdp.ProxyPutRequest(
                    destination_entity_id=source_controller.local_entity_id,
                    source_filename=source_filename,
                    destination_filename=destination_filename)])
