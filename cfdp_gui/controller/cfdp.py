import os
import time
from datetime import datetime

import wx

import cfdp
from cfdp import CfdpEntity
from cfdp.filestore import NativeFileStore
from cfdp.transport.udp import UdpTransport
try:
    from cfdp.transport.zmq import ZmqTransport
except ImportError:
    pass

from .base import BaseController


class CfdpController(BaseController):

    def __init__(self, parent):
        self.parent = parent
        self.current_path = ""
        self.cfdp_user = None
        self.local_entity_id = None
        self.remote_entity_id = None
        self.connected = False
        self.mode = None

    def connect(self, config):
        self.local_entity_id = int(config['local entity id'])
        local_ip, local_port = config['local url'].split(":")
        local_port = int(local_port)
        self.remote_entity_id = int(config['remote entity id'])
        remote_ip, remote_port = config['remote url'].split(":")
        remote_port = int(remote_port)
        rootpath = config['rootpath']

        if config['transport'] == 'zmq':
            try:
                self.transport = ZmqTransport(routing={"*": [(local_ip, local_port)]})
            except NameError:
                print("ZmqTransport not available. Run 'pip install zmq' to fix.")
        elif config['transport'] == 'udp':
            self.transport = UdpTransport(routing={"*": [(local_ip, local_port)]})
        else:
            raise Exception()

        if config['mode'] == 'unacknowledged':
            self.mode = cfdp.TransmissionMode.UNACKNOWLEDGED
        elif config['mode'] == 'acknowledged':
            self.mode = cfdp.TransmissionMode.ACKNOWLEDGED
        else:
            raise Exception()

        self.cfdp_entity = MyCfdpEntity(
            entity_id=self.local_entity_id, 
            filestore=NativeFileStore(rootpath),
            transport=self.transport,
            transactions_panel=self.parent.main_panel.transactions_panel)

        self.transport.bind(remote_ip,remote_port)
        self.connected = True

    def reset_path(self):
        self.current_path = ""

    def disconnect(self):
        if self.connected:
            #while self.cfdp_entity.machines:
            #    time.sleep(0)
            self.cfdp_entity.shutdown()
            self.transport.unbind()
            self.transport.disconnect()
            self.transport.shutdown()
        self.connected = False

    def get_current_virtual_path(self):
        return self.current_path

    def list_directory(self):
        if not self.connected:
            return []

        # local native temporary file that holds remote directory listing
        TMP_FILE = ".listing.remote"

        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            messages_to_user=[
                cfdp.DirectoryListingRequest(
                    remote_directory=self.current_path,
                    local_file=TMP_FILE)])

        # Hack: wait for file to arrive
        while not os.path.isfile(TMP_FILE):
            time.sleep(0.1)

        # Hack: wait for file being completely written
        time.sleep(0.2)

        with open(TMP_FILE) as f:
            lines = f.readlines()
        os.remove(TMP_FILE)

        listing = []
        # go through listing, skip header line
        for line in lines[1:]:
            col = line.strip().split(',')
            type = col[0]
            name = col[1].split('/')[-1]  # strip parent virtual path
            size = col[2]
            modified = datetime.fromtimestamp(
                float(col[3])).strftime('%Y-%m-%d %H:%M:%S')
            listing.append((type, name, size, modified))

        # do not show hidden folders or files
        listing = [x for x in listing if not x[1].startswith('.')]
        # sort by folders and name
        listing.sort(key=lambda x: (x[0], x[1]))

        # add parent directory
        if self.current_path != "":
            listing.insert(0, ('d', '..', "---", "---"))
        return listing

    def enter_directory(self, directory):
        if directory == "..":
            if self.current_path != "":
                self.current_path = "/".join(
                    self.current_path.split('/')[0:-1])
        else:
            if self.current_path == "" or self.current_path == "/":
                self.current_path = "/" + directory
            else:
                self.current_path += '/' + directory

    def create_file(self, name):
        filepath = "/".join([self.current_path, name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(
                    cfdp.ActionCode.CREATE_FILE, filepath)])

    def delete_file(self, name):
        filepath = "/".join([self.current_path, name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(cfdp.ActionCode.DELETE_FILE, filepath)])

    def rename_file(self, old_name, new_name):
        old_filepath = "/".join([self.current_path, old_name])
        new_filepath = "/".join([self.current_path, new_name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(
                    cfdp.ActionCode.RENAME_FILE, old_filepath, new_filepath)])

    def append_file(self, first_name, second_name):
        first_filepath = "/".join([self.current_path, first_name])
        second_filepath = "/".join([self.current_path, second_name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(
                    cfdp.ActionCode.APPEND_FILE,
                    first_filepath, second_filepath)])

    def replace_file(self, first_name, second_name):
        first_filepath = "/".join([self.current_path, first_name])
        second_filepath = "/".join([self.current_path, second_name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(
                    cfdp.ActionCode.REPLACE_FILE,
                    first_filepath, second_filepath)])

    def create_directory(self, name):
        dirpath = "/".join([self.current_path, name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(
                    cfdp.ActionCode.CREATE_DIRECTORY, dirpath)])

    def remove_directory(self, name):
        dirpath = "/".join([self.current_path, name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(
                    cfdp.ActionCode.DENY_DIRECTORY, dirpath)])

    def deny_file(self, name):
        filepath = "/".join([self.current_path, name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(cfdp.ActionCode.DENY_FILE, filepath)])

    def deny_directory(self, name):
        filepath = "/".join([self.current_path, name])
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            filestore_requests=[
                cfdp.FilestoreRequest(
                    cfdp.ActionCode.DENY_DIRECTORY, filepath)])

    def rename_directory(self, old_name, new_name):
        raise Exception("Directory renaming is not supported by CFDP")

    def suspend(self, transaction_id):
        wx.LogMessage("Suspend {}".format(transaction_id))
        if transaction_id[0] == self.cfdp_entity.entity_id:
            self.cfdp_entity.suspend(transaction_id)
        else:
            # TODO
            wx.LogError("Not implemented")

    def resume(self, transaction_id):
        wx.LogMessage("Resume {}".format(transaction_id))
        if transaction_id[0] == self.cfdp_entity.entity_id:
            self.cfdp_entity.resume(transaction_id)
        else:
            # TODO
            wx.LogError("Not implemented")

    def cancel(self, transaction_id):
        wx.LogMessage("Cancel {}".format(transaction_id))
        if transaction_id[0] == self.cfdp_entity.entity_id:
            self.cfdp_entity.cancel(transaction_id)
        else:
            # TODO
            wx.LogError("Not implemented")

    def freeze(self):
        wx.LogMessage("Freeze All")
        for transaction_id, machine in self.cfdp_entity.machines.items():
            local_entity_id = machine.transaction.source_entity_id
            remote_entity_id = machine.transaction.destination_entity_id
            if self.cfdp_entity.entity_id == local_entity_id:
                self.cfdp_entity.freeze(remote_entity_id)

    def thaw(self):
        wx.LogMessage("Thaw All")
        for transaction_id, machine in self.cfdp_entity.machines.items():
            local_entity_id = machine.transaction.source_entity_id
            remote_entity_id = machine.transaction.destination_entity_id
            if self.cfdp_entity.entity_id == local_entity_id:
                self.cfdp_entity.thaw(remote_entity_id)

    def remote_suspend(self, transaction_id):
        wx.LogMessage("Remote Suspend {}".format(transaction_id))
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            messages_to_user=[cfdp.RemoteSuspendRequest(*transaction_id)])

    def remote_resume(self, transaction_id):
        wx.LogMessage("Remote Resume {}".format(transaction_id))
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            messages_to_user=[cfdp.RemoteResumeRequest(*transaction_id)])

    def remote_cancel(self, transaction_id):
        wx.LogMessage("Remote Cancel {}".format(transaction_id))
        self.cfdp_entity.put(
            destination_id=self.remote_entity_id,
            transmission_mode=self.mode,
            messages_to_user=[cfdp.ProxyPutCancel(*transaction_id)])


class MyCfdpEntity(CfdpEntity):

    def __init__(self, *args, transactions_panel=None, **kwargs):
        super().__init__(*args, **kwargs)
        if not transactions_panel:
            raise Exception()
        self.transactions_panel = transactions_panel

    def transaction_indication(self, transaction_id):
        super().transaction_indication(transaction_id)
        wx.LogMessage("[%s] Transaction indication" % str(transaction_id))
        self.transactions_panel.transaction_indication(transaction_id)

    def eof_sent_indication(self, transaction_id):
        super().eof_sent_indication(transaction_id)
        wx.LogMessage("[%s] EOF sent indication" % str(transaction_id))
        self.transactions_panel.eof_sent_indication(transaction_id)

    def transaction_finished_indication(
            self, transaction_id, condition_code, file_status, delivery_code,
            filestore_responses=None, status_report=None):
        super().transaction_finished_indication(
            transaction_id, condition_code, file_status, delivery_code,
            filestore_responses, status_report)
        wx.LogMessage(
            "[%s] Transaction finished indication" % str(transaction_id))
        self.transactions_panel.transaction_finished_indication(
            transaction_id)

    def metadata_received_indication(
            self, transaction_id, source_entity_id, file_size=None,
            source_filename=None, destination_filename=None,
            messages_to_user=None):
        super().metadata_received_indication(
            transaction_id, source_entity_id, file_size, source_filename,
            destination_filename, messages_to_user)
        wx.LogMessage(
            "[%s] Metadata received indication" % str(transaction_id))
        self.transactions_panel.metadata_received_indication(transaction_id)

    def filesegment_received_indication(self, transaction_id, offset, length):
        super().filesegment_received_indication(transaction_id, offset, length)
        wx.LogMessage(
            "[%s] Filesegment received indication" % str(transaction_id))

    def report_indication(self, transaction_id, status_report):
        super().report_indication(transaction_id, status_report)
        wx.LogMessage("[%s] Report indication" % str(transaction_id))

    def suspended_indication(self, transaction_id, condition_code):
        super().suspended_indication(transaction_id, condition_code)
        wx.LogMessage("[%s] Suspended indication" % str(transaction_id))
        self.transactions_panel.suspended_indication(transaction_id)

    def resumed_indication(self, transaction_id, progress):
        super().resumed_indication(transaction_id, progress)
        wx.LogMessage("[%s] Resumed indication" % str(transaction_id))
        self.transactions_panel.resumed_indication(transaction_id)

    def fault_indication(self, transaction_id, condition_code, progress):
        super().fault_indication(transaction_id, condition_code, progress)
        wx.LogMessage("[%s] Fault indication" % str(transaction_id))

    def abandoned_indication(self, transaction_id, condition_code, progress):
        super().abandoned_indication(transaction_id, condition_code, progress)
        wx.LogMessage("[%s] Abandoned indication" % str(transaction_id))

    def eof_received_indication(self, transaction_id):
        super().eof_received_indication(transaction_id)
        wx.LogMessage("[%s] EOF received indication" % str(transaction_id))
