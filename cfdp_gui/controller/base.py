

class BaseController:

    def __init__(self, parent):
        self.parent = parent

    def get_current_virtual_path(self):
        raise NotImplementedError

    def list_directory(self):
        raise NotImplementedError

    def enter_directory(self, directory):
        raise NotImplementedError

    def create_file(self, name):
        raise NotImplementedError

    def delete_file(self, name):
        raise NotImplementedError

    def rename_file(self, old_name, new_name):
        raise NotImplementedError

    def append_file(self, first_name, second_name):
        raise NotImplementedError

    def replace_file(self, first_name, second_name):
        raise NotImplementedError

    def create_directory(self, name):
        raise NotImplementedError

    def remove_directory(self, name):
        raise NotImplementedError

    def deny_file(self, name):
        raise NotImplementedError

    def deny_directory(self, name):
        raise NotImplementedError
