import threading
from ast import literal_eval as make_tuple

import wx
import wx.aui

from .controller.transfer_file import transfer_file
from .controller.native import NativeController
from .controller.cfdp import CfdpController


PROPORTION = 1
BORDER = 5
VGAP = 0
HGAP = 0


class MainPanel(wx.Panel):

    def __init__(self, parent, local_controller, remote_controller):
        super().__init__(parent)
        self.parent = parent
        self.local_controller = local_controller
        self.remote_controller = remote_controller

        self.mgr = wx.aui.AuiManager(self)

        """ Log panel """

        self.log_panel = wx.Panel(self, style=wx.SIMPLE_BORDER)
        log_panel_info = wx.aui.AuiPaneInfo()
        log_panel_info.Top()
        log_panel_info.CloseButton(visible=False)
        log_panel_info.MinSize(100, 100)
        self.mgr.AddPane(self.log_panel, log_panel_info)

        log_text = wx.TextCtrl(
            self.log_panel, style=(
                wx.TE_MULTILINE | wx.TE_READONLY | wx.HSCROLL))
        log_sizer = wx.BoxSizer(wx.VERTICAL)
        log_sizer.Add(log_text, PROPORTION, wx.EXPAND | wx.ALL, BORDER)
        self.log_panel.SetSizer(log_sizer)
        wx.Log.SetActiveTarget(wx.LogTextCtrl(log_text))

        """ File panel """

        self.files_panel = wx.Panel(self, style=wx.SIMPLE_BORDER)
        files_panel_info = wx.aui.AuiPaneInfo()
        files_panel_info.Centre()
        files_panel_info.CloseButton(visible=False)
        files_panel_info.MinSize(100, 100)
        self.mgr.AddPane(self.files_panel, files_panel_info)

        self.files_sizer = wx.GridSizer(1, 2, VGAP, HGAP)
        self.files_panel.SetSizer(self.files_sizer)
        self.left_pane = FileExplorer(
            self.files_panel,
            controller=self.local_controller,
            other_controller=self.remote_controller)

        self.files_sizer.Add(
            self.left_pane, PROPORTION, wx.EXPAND | wx.RIGHT, BORDER)

        """ Transaction panel """

        self.transactions_panel = TransactionPanel(self)
        transactions_panel_info = wx.aui.AuiPaneInfo()
        transactions_panel_info.Bottom()
        transactions_panel_info.CloseButton(visible=False)
        transactions_panel_info.MinSize(100, 100)
        self.mgr.AddPane(self.transactions_panel, transactions_panel_info)

        self.mgr.Update()

        self.parent.Bind(wx.EVT_CLOSE, self.on_close)

    def on_connect(self, config):
        self.remote_controller.connect(config)

        self.right_pane = FileExplorer(
            self.files_panel,
            controller=self.remote_controller,
            other_controller=self.local_controller)

        self.files_sizer.Add(
            self.right_pane, PROPORTION, wx.EXPAND | wx.LEFT, BORDER)
        self.mgr.Update()

    def on_disconnect(self):
        self.right_pane.Show(False)
        self.files_sizer.Remove(1)
        self.remote_controller.disconnect()
        self.remote_controller.reset_path()
        self.transactions_panel.clear_transactions()

    def on_close(self, event):
        self.remote_controller.disconnect()
        # close and destroy panel manager and elevate event
        self.mgr.UnInit()
        self.Destroy()
        event.Skip()


class TransactionPanel(wx.Panel):

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent

        self.sizer = wx.GridSizer(1, 2, 0, 0)
        self.SetSizer(self.sizer)

        self.transactions = {}

        self.local_transactions_list = wx.ListCtrl(
            self, wx.NewIdRef(), style=wx.LC_REPORT)
        self.local_transactions_list.InsertColumn(0, 'ID')
        self.local_transactions_list.InsertColumn(1, 'Status')
        self.local_transactions_list.InsertColumn(2, 'Progress')

        self.remote_transactions_list = wx.ListCtrl(
            self, wx.NewIdRef(), style=wx.LC_REPORT)
        self.remote_transactions_list.InsertColumn(0, 'ID')
        self.remote_transactions_list.InsertColumn(1, 'Status')
        self.remote_transactions_list.InsertColumn(2, 'Progress')

        self.sizer.Add(self.local_transactions_list, 0, wx.EXPAND)
        self.sizer.Add(self.remote_transactions_list, 0, wx.EXPAND)

        self.local_transactions_list.Bind(
            wx.EVT_RIGHT_DOWN, self.on_context_menu_local)

        self.remote_transactions_list.Bind(
            wx.EVT_RIGHT_DOWN, self.on_context_menu_remote)

    def transaction_indication(self, transaction_id):
        self.transactions[transaction_id] = 'running'
        self.update_transaction_list()

    def eof_sent_indication(self, transaction_id):
        self.transactions[transaction_id] = 'EOF sent'

    def transaction_finished_indication(self, transaction_id):
        self.transactions.pop(transaction_id)
        self.update_transaction_list()

    def metadata_received_indication(self, transaction_id):
        self.transactions[transaction_id] = 'running'
        self.update_transaction_list()

    def suspended_indication(self, transaction_id):
        self.transactions[transaction_id] = 'suspended'
        self.update_transaction_list()

    def resumed_indication(self, transaction_id):
        self.transactions[transaction_id] = 'running'
        self.update_transaction_list()

    def clear_transactions(self):
        self.transactions = {}
        self.local_transactions_list.DeleteAllItems()
        self.remote_transactions_list.DeleteAllItems()

    def update_transaction_list(self):
        self.local_transactions_list.DeleteAllItems()
        self.remote_transactions_list.DeleteAllItems()
        for transaction_id, status in self.transactions.items():
            if transaction_id[0] ==\
                    self.parent.remote_controller.local_entity_id:
                self.local_transactions_list.Append(
                    [str(transaction_id), status])
            else:
                self.remote_transactions_list.Append(
                    [str(transaction_id), status])

    def on_context_menu_local(self, event):
        self.local_transactions_list.PopupMenu(
            LocalTransactionContextMenu(self), event.GetPosition())

    def on_context_menu_remote(self, event):
        self.remote_transactions_list.PopupMenu(
            RemoteTransactionContextMenu(self), event.GetPosition())


class FileExplorer(wx.Panel):

    def __init__(self, parent, controller, other_controller):
        super().__init__(parent)
        self.parent = parent
        self.controller = controller
        self.other_controller = other_controller
        self.update_file_list_lock = threading.Lock()

        self.sizer = wx.BoxSizer(orient=wx.VERTICAL)
        self.SetSizer(self.sizer)

        self.file_list = wx.ListCtrl(self, style=wx.LC_REPORT)
        self.file_list.AppendColumn('', format=wx.LIST_FORMAT_LEFT)
        self.file_list.AppendColumn('Name', format=wx.LIST_FORMAT_LEFT)
        self.file_list.AppendColumn('Size', format=wx.LIST_FORMAT_LEFT)
        self.file_list.AppendColumn('Mod Date', format=wx.LIST_FORMAT_LEFT)
        self.drop_target = TextDropTarget(self, self.file_list)
        self.file_list.SetDropTarget(self.drop_target)
        self.sizer.Add(self.file_list, 1, wx.EXPAND)

        self.file_list.Bind(
            wx.EVT_LIST_ITEM_ACTIVATED, self.on_item_action)
        self.file_list.Bind(
            wx.EVT_RIGHT_DOWN, self.on_context_menu)
        wx.EvtHandler.Bind(
            self, wx.EVT_LIST_BEGIN_DRAG,
            self.on_drag_init, id=self.file_list.GetId())

        self.update_file_list()

    def update_file_list(self):
        self.file_list.DeleteAllItems()
        threading.Thread(target=self._update_file_list).start()

    def _update_file_list(self):
        with self.update_file_list_lock:
            for entry in self.controller.list_directory():
                self.file_list.Append([entry[0], entry[1], entry[2], entry[3]])
            self.file_list.SetColumnWidth(0, wx.LIST_AUTOSIZE)
            self.file_list.SetColumnWidth(1, wx.LIST_AUTOSIZE)
            self.file_list.SetColumnWidth(2, wx.LIST_AUTOSIZE)
            self.file_list.SetColumnWidth(3, wx.LIST_AUTOSIZE)

    def on_item_action(self, event):
        # if double-clicked on folder then enter it, otherwise do nothing
        if self.file_list.GetItemText(event.GetIndex(), 0) == 'd':
            folder = self.file_list.GetItemText(event.GetIndex(), 1)
            self.controller.enter_directory(folder)
            self.update_file_list()

    def on_context_menu(self, event):
        self.PopupMenu(FileContextMenu(self), event.GetPosition())

    def on_drag_init(self, event):
        is_file = self.file_list.GetItemText(event.GetIndex(), 0) == 'f'
        if is_file:
            controller_text = ''
            if isinstance(self.controller, CfdpController):
                controller_text = 'cfdp'
            elif isinstance(self.controller, NativeController):
                controller_text = 'local'
            text = "{}:{}".format(
                controller_text,
                self.file_list.GetItemText(event.GetIndex(), 1))
            tobj = wx.TextDataObject(text)
            src = wx.DropSource(self.file_list)
            src.SetData(tobj)
            src.DoDragDrop(True)


class FileContextMenu(wx.Menu):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        item_refresh = wx.MenuItem(self, wx.NewIdRef(), text='Refresh')
        self.Bind(wx.EVT_MENU, self._on_refresh, item_refresh)
        self.Append(item_refresh)

        item_rename = wx.MenuItem(self, wx.NewIdRef(), text='Rename')
        self.Bind(wx.EVT_MENU, self._on_rename, item_rename)
        self.Append(item_rename)

        item_delete = wx.MenuItem(self, wx.NewIdRef(), 'Delete')
        self.Bind(wx.EVT_MENU, self._on_delete, item_delete)
        self.Append(item_delete)

        item_create_file = wx.MenuItem(self, wx.NewIdRef(), 'Create File')
        self.Bind(wx.EVT_MENU, self._on_create_file, item_create_file)
        self.Append(item_create_file)

        item_create_directory = wx.MenuItem(
            self, wx.NewIdRef(), 'Create Directory')
        self.Bind(
            wx.EVT_MENU, self._on_create_directory, item_create_directory)
        self.Append(item_create_directory)

        item_append_file = wx.MenuItem(self, wx.NewIdRef(), 'Append Files')
        self.Bind(wx.EVT_MENU, self._on_append_file, item_append_file)
        self.Append(item_append_file)

        item_replace_file = wx.MenuItem(self, wx.NewIdRef(), 'Replace Files')
        self.Bind(wx.EVT_MENU, self._on_replace_file, item_replace_file)
        self.Append(item_replace_file)

    def _on_refresh(self, event):
        self.parent.update_file_list()

    def _on_append_file(self, event):
        item = self.parent.file_list.GetFirstSelected()
        item1_name = self.parent.file_list.GetItemText(item, 1)
        item_2 = self.parent.file_list.GetNextSelected(item)
        item2_name = self.parent.file_list.GetItemText(item_2, 1)
        if (item < 0 or
                self.parent.file_list.GetNextSelected(item) < 0 or
                self.parent.file_list.GetItemText(item, 1) == '..' or
                self.parent.file_list.GetItemText(item, 0) == 'd' or
                self.parent.file_list.GetItemText(item_2, 0) == 'd'):
            # do nothing if two files not selected,parent dir (..) is selected
            # or any directory is selected
            return
        else:
            self.parent.controller.append_file(item1_name, item2_name)
            self.parent.update_file_list()

    def _on_replace_file(self, event):
        item = self.parent.file_list.GetFirstSelected()
        item1_name = self.parent.file_list.GetItemText(item, 1)
        item_2 = self.parent.file_list.GetNextSelected(item)
        item2_name = self.parent.file_list.GetItemText(item_2, 1)
        if (item < 0 or
                self.parent.file_list.GetNextSelected(item) < 0 or
                self.parent.file_list.GetItemText(item, 1) == '..' or
                self.parent.file_list.GetItemText(item, 0) == 'd' or
                self.parent.file_list.GetItemText(item_2, 0) == 'd'):
            # do nothing if two files not selected,parent dir (..) is selected
            # or any directory is selected
            return
        else:
            self.parent.controller.replace_file(item1_name, item2_name)
            self.parent.update_file_list()

    def _on_rename(self, event):
        item = self.parent.file_list.GetFirstSelected()
        if item < 0 or self.parent.file_list.GetItemText(item, 1) == '..':
            # do nothing if no file(s) selected or parent dir (..) is selected
            return

        old_name = self.parent.file_list.GetItemText(item, 1)
        is_file = self.parent.file_list.GetItemText(item, 0) == 'f'
        if is_file:  # directory renaming is not allowed
            with RenameDialog(self.parent, old_name) as dlg:
                dlg.SetValue(old_name)
                if dlg.ShowModal() == wx.ID_OK:
                    new_name = dlg.GetValue()
                    self.parent.controller.rename_file(old_name, new_name)
                    self.parent.update_file_list()

    def _on_delete(self, event):
        item = self.parent.file_list.GetFirstSelected()
        if item < 0 or self.parent.file_list.GetItemText(item, 1) == '..':
            # do nothing if no file(s) selected or parent dir (..) is selected
            return

        while item >= 0:
            item_name = self.parent.file_list.GetItemText(item, 1)
            is_file = self.parent.file_list.GetItemText(item, 0) == 'f'
            if is_file:
                self.parent.controller.delete_file(item_name)
            else:
                self.parent.controller.remove_directory(item_name)

            item = self.parent.file_list.GetNextSelected(item)
        self.parent.update_file_list()

    def _on_create_directory(self, event):
        with CreateDirectoryDialog(self.parent) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                new_name = dlg.GetValue()
                self.parent.controller.create_directory(new_name)
                self.parent.update_file_list()

    def _on_create_file(self, event):
        with CreateFileDialog(self.parent) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                new_name = dlg.GetValue()
                self.parent.controller.create_file(new_name)
                self.parent.update_file_list()


class TransactionContextMenu(wx.Menu):

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.type = type

        item_cancel = wx.MenuItem(self, wx.NewIdRef(), text='Cancel')
        self.Bind(wx.EVT_MENU, self._on_cancel, item_cancel)
        self.Append(item_cancel)

        item_suspend = wx.MenuItem(self, wx.NewIdRef(), text='Suspend')
        self.Bind(wx.EVT_MENU, self._on_suspend, item_suspend)
        self.Append(item_suspend)

        item_resume = wx.MenuItem(self, wx.NewIdRef(), text='Resume')
        self.Bind(wx.EVT_MENU, self._on_resume, item_resume)
        self.Append(item_resume)

        item_freeze = wx.MenuItem(self, wx.NewIdRef(), text='Freeze All')
        self.Bind(wx.EVT_MENU, self._on_freeze, item_freeze)
        self.Append(item_freeze)

        item_thaw = wx.MenuItem(self, wx.NewIdRef(), text='Thaw All')
        self.Bind(wx.EVT_MENU, self._on_thaw, item_thaw)
        self.Append(item_thaw)

    def _on_suspend(self, event):
        n = self.transactions_list.GetFirstSelected()
        if n < 0:
            return
        transaction_id = make_tuple(
            self.transactions_list.GetItem(n).Text)
        while n != -1:
            self.controller_suspend(transaction_id)
            n = self.transactions_list.GetNextSelected(n)
        self.parent.update_transaction_list()

    def _on_resume(self, event):
        n = self.transactions_list.GetFirstSelected()
        if n < 0:
            return
        transaction_id = make_tuple(
            self.transactions_list.GetItem(n).Text)
        while n != -1:
            self.controller_resume(transaction_id)
            n = self.transactions_list.GetNextSelected(n)
        self.parent.update_transaction_list()

    def _on_cancel(self, event):
        n = self.transactions_list.GetFirstSelected()
        if n < 0:
            return
        transaction_id = make_tuple(self.transactions_list.GetItem(n).Text)
        while n != -1:
            self.controller_cancel(transaction_id)
            n = self.transactions_list.GetNextSelected(n)
        self.parent.update_transaction_list()

    def _on_freeze(self, event):
        n = self.transactions_list.GetFirstSelected()
        if n < 0:
            return
        self.parent.parent.remote_controller.freeze()

    def _on_thaw(self, event):
        n = self.transactions_list.GetFirstSelected()
        if n < 0:
            return
        self.parent.parent.remote_controller.thaw()


class LocalTransactionContextMenu(TransactionContextMenu):

    def __init__(self, parent):
        super().__init__(parent)
        self.transactions_list = self.parent.local_transactions_list
        self.controller_suspend =\
            self.parent.parent.remote_controller.suspend
        self.controller_resume =\
            self.parent.parent.remote_controller.resume
        self.controller_cancel =\
            self.parent.parent.remote_controller.cancel


class RemoteTransactionContextMenu(TransactionContextMenu):

    def __init__(self, parent):
        super().__init__(parent)
        self.transactions_list = self.parent.remote_transactions_list
        self.controller_suspend =\
            self.parent.parent.remote_controller.remote_suspend
        self.controller_resume =\
            self.parent.parent.remote_controller.remote_resume
        self.controller_cancel =\
            self.parent.parent.remote_controller.remote_cancel


class TextDropTarget(wx.TextDropTarget):

    def __init__(self, parent, object):
        super().__init__()
        self.parent = parent
        self.object = object

    def OnDropText(self, x, y, name):
        controller, name = name.split(':')
        if isinstance(self.parent.controller, CfdpController) and\
                controller == 'local' or\
                isinstance(self.parent.controller, NativeController) and\
                controller == 'cfdp':
            transfer_file(
                name, self.parent.controller, self.parent.other_controller)
        return True


class RenameDialog(wx.TextEntryDialog):

    def __init__(self, parent, old_name):
        super().__init__(
            parent, "Enter new filename",
            caption="Rename {}".format(old_name))


class CreateDirectoryDialog(wx.TextEntryDialog):

    def __init__(self, parent):
        super().__init__(
            parent, "Enter name for new directory",
            caption="Enter name for new directory")


class CreateFileDialog(wx.TextEntryDialog):

    def __init__(self, parent):
        super().__init__(
            parent, "Enter name for new file",
            caption="Enter name for new file")
