# Python CFDP GUI

A basic GUI application that looks similar to your typical FTP file transfer
application, but uses the Python CFDP protocol (https://gitlab.com/librecube/prototypes/python-cfdp) to exchange files and meta information.

![Screenshot](docs/screenshot.png)

## Getting Started

Install the GUI application via pip:

```bash
$ pip install git+https://gitlab.com/librecube/prototypes/python-cfdp-gui
```

Configure a config.yaml which is located in the same directory like the run.py.
(see `examples/config.yaml)

``` yaml
transport: udp
local entity id: '2'
local url: 127.0.0.1:5552
remote entity id: '1'
remote url: 127.0.0.1:5551
rootpath: os.getcwd()
mode: unacknowledged
```

Then start the CFDP GUI in a Python terminal (see `examples/run.py`):

```python
from cfdp_gui import App
App.run()
```

In order to connect to a remote CFDP entity (ie. CFDP server) you need to
have one running. To start one locally, you can use a test server
(see `examples/server.py`):

```python
import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(
        1, "127.0.0.1:5551"),
    remote_entities=[cfdp.RemoteEntity(
        2, "127.0.0.1:5552")],
    filestore=NativeFileStore("."),
    transport=UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)
cfdp_entity.transport.bind()
input("Running. Press <Enter> to stop...\n")
cfdp_entity.transport.unbind()
cfdp_entity.shutdown()
```
